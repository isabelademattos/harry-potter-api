class CreateCharacters < ActiveRecord::Migration[6.0]
  def change
    create_table :characters, id: :uuid, default: 'gen_random_uuid()' do |t|
      t.string :name
      t.string :role
      t.string :school
      t.string :house
      t.string :patronus

      t.timestamps
    end
  end
end
