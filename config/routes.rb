Rails.application.routes.draw do
  # swagger engine
  mount Rswag::Ui::Engine => '/api-docs'
  mount Rswag::Api::Engine => '/api-docs'

  root 'api/v1/characters#index'

  namespace 'api' do
    namespace 'v1' do
      # routes from characters
      resources :characters
      # route to get all characters by house
      get 'characters/houses/:house', to: 'characters#characters_by_house'
      # routes to external API
      get 'houses', to: 'characters#houses_all'
      get 'houses/:house', to: 'characters#houses_by_id'
    end
  end
end
