require 'rails_helper'

RSpec.describe 'Characters API', type: :request do
  let!(:characters) { create_list(:character, 10) }
  let(:character_id) { characters.first.id }

  context 'GET /api/v1/characters' do
    before { get '/api/v1/characters' }

    it 'returns characters' do
      expect(json).not_to be_empty
      expect(json.size).to eq(10)
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end

  context 'GET /api/v1/characters/:id' do
    before { get "/api/v1/characters/#{character_id}" }

    context 'when the record exists' do
      it 'returns the character' do
        expect(json).not_to be_empty
        expect(json['id']).to eq(character_id)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the record does not exist' do
      let(:character_id) { 'bb7fc5bb-05c8-4c2e-8d02-d30c499600d4' }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Character/)
      end
    end
  end

  context 'POST /api/v1/characters' do
    let(:valid_attributes) { { name: 'Harry Potter',
                               role: 'student',
                               school: 'Hogwarts',
                               house: '5a05e2b252f721a3cf2ea33f',
                               patronus: 'stag' } }

    context 'when the request is valid' do
      before { post '/api/v1/characters', params: valid_attributes }

      it 'creates a character' do
        expect(json['name']).to eq('Harry Potter')
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when the request is invalid' do
      before { post '/api/v1/characters', params: { name: 'Hermione' } }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a validation failure message' do
        expect(response.body)
          .to match(/A validação falhou: Posição não pode ficar em branco, Escola não pode ficar em branco, Casa não pode ficar em branco, Patrono não pode ficar em branco/)
      end
    end
  end

  context 'PUT /api/v1/characters/:id' do
    let(:valid_attributes) { { name: 'Harry Potter',
                               role: 'student',
                               school: 'Hogwarts',
                               house: '5a05e2b252f721a3cf2ea33f',
                               patronus: 'stag' } }
    context 'when the record exists' do
      context 'when the request is valid' do
        before { put "/api/v1/characters/#{character_id}", params: valid_attributes }

        it 'updates a character' do
          expect(response.body).to be_empty
        end

        it 'returns status code 204' do
          expect(response).to have_http_status(204)
        end
      end
    end

    context 'when the record does not exist' do
      let(:invalid_id) { '7c64264e-0f9a' }

      before { put "/api/v1/characters/#{invalid_id}", params: valid_attributes }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end
    end
  end

  context 'DELETE /api/v1/characters/:id' do
    context 'when the record exists' do
      before { delete "/api/v1/characters/#{character_id}" }

      it 'returns status code 204' do
        expect(response).to have_http_status(204)
      end
    end

    context 'when the record does not exist' do
      let(:invalid_id) { '7c64264e-0f9a' }

      before { delete "/api/v1/characters/#{invalid_id}" }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end
    end
  end
end
