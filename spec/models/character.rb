require 'rails_helper'

RSpec.describe Character, type: :model do
  let!(:character) { create_list(:character, 1) }

  context 'validations' do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:role) }
    it { should validate_presence_of(:school) }
    it { should validate_presence_of(:house) }
    it { should validate_presence_of(:patronus) }

    it { should validate_length_of(:name).is_at_most(255) }
    it { should validate_length_of(:role).is_at_most(255) }
    it { should validate_length_of(:school).is_at_most(255) }
    it { should validate_length_of(:house).is_at_most(255) }
    it { should validate_length_of(:patronus).is_at_most(255) }
  end
end
