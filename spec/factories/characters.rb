FactoryBot.define do
  factory :character do
    name { Faker::Name.name }
    role { Faker::Lorem.word  }
    school { Faker::Name.name }
    house { Faker::Lorem.word }
    patronus { Faker::Name.name }
  end
end
