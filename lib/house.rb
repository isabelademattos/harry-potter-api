module House
  include HTTParty

  # GET all houses from external API
  def self.get_all
    response = get("#{ENV['POTTER_API_URL']}v1/houses?key=#{ENV['POTTER_API_KEY']}",
      headers: {'Content-Type' => 'application/json'})
  end

  # GET house by id from external API
  def self.get_by_id(house)
    response = get("#{ENV['POTTER_API_URL']}v1/houses/#{house}?key=#{ENV['POTTER_API_KEY']}",
      headers: {'Content-Type' => 'application/json'})
  end
end
