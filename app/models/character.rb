class Character < ApplicationRecord
  include House

  validates_presence_of :name, :role, :school, :house, :patronus

  validates :name, length: { maximum: 255 }
  validates :role, length: { maximum: 255 }
  validates :school, length: { maximum: 255 }
  validates :house, length: { maximum: 255 }
  validates :patronus, length: { maximum: 255 }

  # validate :invalid_house

  scope :by_house, -> (house) { where('house = ?', "#{house}") }

  # check if the passed house ir valid
  def invalid_house
    response = House.get_by_id(house)
    errors.add(:invalid_house) if response[0].blank?
  end
end
