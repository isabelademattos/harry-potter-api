class Api::V1::CharactersController < ApplicationController
  include HouseHelper

  before_action :set_character, only: [:show, :update, :destroy]

  # GET /api/v1/characters
  def index
    @characters = Character.order('created_at DESC')
    json_response(@characters)
  end

  # GET /api/v1/characters/:character_id
  def show
    json_response(@character)
  end

  # POST /api/v1/characters
  def create
    @character = Character.create!(character_params)
    json_response(@character, :created)
  end


  # PATCH/PUT /api/v1/characters/:character_id
  def update
    @character.update!(character_params)
    head :no_content
  end

  # DELETE /api/v1/characters/:character_id
  def destroy
    @character.destroy!
    head :no_content
  end

  # GET /api/v1/characters/houses/:house_id
  def characters_by_house
    @characters = Character.by_house(params[:house])
    json_response(@characters)
  end

  # GET /api/v1/houses
  def houses_all
    fetch_houses
  end

  # GET /api/v1/houses/:house_id
  def houses_by_id
    @house = House.get_by_id(params[:house])
    json_response(@house)
  end

  private
  # use callbacks to share common setup or constraints between actions
  def set_character
    @character = Character.find(params[:id])
  end

  # only allow a trusted parameters in white list
  def character_params
    params.permit(:name, :role, :school, :house, :patronus)
  end
end
