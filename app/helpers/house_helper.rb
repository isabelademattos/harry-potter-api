module HouseHelper
  include House

  # puts data in redis if doesn't exists
  def fetch_houses
    houses = $redis.get("houses")
    if houses.blank?
      houses = House.get_all
      $redis.set("houses", houses.to_json)
    end
    json_response(houses)
  end
end
