{
  "openapi": "3.0.1",
  "info": {
    "title": "Harry Potter API",
    "description": "Welcome to Harry Potter API",
    "version": "v1"
  },
  "paths": {
    "/api/v1/characters": {
      "get": {
        "tags": [
          "Characters"
        ],
        "summary": "Get all Characters",
        "description": "Sample request:\r\n\r\n curl -X GET \"api/v1/characters\" -H \"accept: text/plain\"",
        "parameters": [],
        "responses": {
          "200": {
            "description": "A list of Characters.",
            "content": {
              "text/plain": {
                "schema": {
                  "$ref": "#/components/schemas/CharactersQueryResponse"
                }
              },
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/CharactersQueryResponse"
                }
              },
              "text/json": {
                "schema": {
                  "$ref": "#/components/schemas/CharactersQueryResponse"
                }
              }
            }
          },
          "404": {
            "description": "Characters not found.",
            "content": {
              "text/plain": {
                "schema": {
                  "$ref": "#/components/schemas/DefaultResponse"
                }
              },
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/DefaultResponse"
                }
              },
              "text/json": {
                "schema": {
                  "$ref": "#/components/schemas/DefaultResponse"
                }
              }
            }
          },
          "500": {
            "description": "Internal server error."
          }
        }
      },
      "post": {
        "tags": [
          "Characters"
        ],
        "summary": "Create a Character.",
        "description": "Sample request:\r\n\r\n POST api/v1/characters\r\n{\r\n\"name\": \"string\",\r\n\"role\": \"string\",\r\n\"school\": \"string\",\r\n\"house\": \"string\",\r\n \"patronus\": \"string\",\r\n }",
        "requestBody": {
          "description": "",
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/CreateCharactersCommand"
              }
            },
            "text/json": {
              "schema": {
                "$ref": "#/components/schemas/CreateCharactersCommand"
              }
            },
            "application/*+json": {
              "schema": {
                "$ref": "#/components/schemas/CreateCharactersCommand"
              }
            }
          }
        },
        "responses": {
          "201": {
            "description": "Accepted for processing.",
            "content": {
              "text/plain": {
                "schema": {
                  "$ref": "#/components/schemas/CreateCharacterCommandResponse"
                }
              },
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/CreateCharacterCommandResponse"
                }
              },
              "text/json": {
                "schema": {
                  "$ref": "#/components/schemas/CreateCharacterCommandResponse"
                }
              }
            }
          },
          "409": {
            "description": "Conflict.",
            "content": {
              "text/plain": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/Notification"
                  }
                }
              },
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/Notification"
                  }
                }
              },
              "text/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/Notification"
                  }
                }
              }
            }
          },
          "412": {
            "description": "Pre condition validation.",
            "content": {
              "text/plain": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/Notification"
                  }
                }
              },
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/Notification"
                  }
                }
              },
              "text/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/Notification"
                  }
                }
              }
            }
          },
          "500": {
            "description": "Internal server error."
          }
        }
      }
    },
    "/api/v1/characters/{id}": {
      "get": {
        "tags": [
          "Characters"
        ],
        "summary": "Get a single Character.",
        "description": "Sample request:\r\n\r\n curl -X GET \"api/v1/characters/3fa85f64-5717-4562-b3fc-2c963f66afa6\" -H \"accept: text/plain\"",
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "description": "",
            "required": true,
            "schema": {
              "type": "string",
              "description": "",
              "format": "uuid"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "A single Character.",
            "content": {
              "text/plain": {
                "schema": {
                  "$ref": "#/components/schemas/CharacterQueryResponse"
                }
              },
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/CharacterQueryResponse"
                }
              },
              "text/json": {
                "schema": {
                  "$ref": "#/components/schemas/CharacterQueryResponse"
                }
              }
            }
          },
          "404": {
            "description": "Character not found.",
            "content": {
              "text/plain": {
                "schema": {
                  "$ref": "#/components/schemas/DefaultResponse"
                }
              },
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/DefaultResponse"
                }
              },
              "text/json": {
                "schema": {
                  "$ref": "#/components/schemas/DefaultResponse"
                }
              }
            }
          },
          "500": {
            "description": "Internal server error."
          }
        }
      },
      "put": {
        "tags": [
          "Characters"
        ],
        "summary": "Update a Character.",
        "description": "Sample request:\r\n\r\n PUT api/v1/characters/a10b44ed-cdfe-4c9e-a76a-72f08d3aef2c\r\n{\r\n\"name\": \"string\",\r\n\"role\": \"string\",\r\n\"school\": \"string\",\r\n\"house\": \"string\",\r\n \"patronus\": \"string\",\r\n }",
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "description": "",
            "required": true,
            "schema": {
              "type": "string",
              "description": "",
              "format": "uuid"
            }
          }
        ],
        "requestBody": {
          "description": "",
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/UpdateCharacterCommand"
              }
            },
            "text/json": {
              "schema": {
                "$ref": "#/components/schemas/UpdateCharacterCommand"
              }
            },
            "application/*+json": {
              "schema": {
                "$ref": "#/components/schemas/UpdateCharacterCommand"
              }
            }
          }
        },
        "responses": {
          "204": {
            "description": "No content.",
            "content": {
              "text/plain": {
                "schema": {
                  "$ref": "#/components/schemas/UpdateCharacterCommandResponse"
                }
              },
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/UpdateCharacterCommandResponse"
                }
              },
              "text/json": {
                "schema": {
                  "$ref": "#/components/schemas/UpdateCharacterCommandResponse"
                }
              }
            }
          },
          "412": {
            "description": "Pre condition validation.",
            "content": {
              "text/plain": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/Notification"
                  }
                }
              },
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/Notification"
                  }
                }
              },
              "text/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/Notification"
                  }
                }
              }
            }
          },
          "500": {
            "description": "Internal server error."
          }
        }
      },
      "delete": {
        "tags": [
          "Characters"
        ],
        "summary": "Delete a Character.",
        "description": "Sample request:\r\n\r\n DELETE api/v1/characters/a10b44ed-cdfe-4c9e-a76a-72f08d3aef2c\r\n",
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "description": "",
            "required": true,
            "schema": {
              "type": "string",
              "description": "",
              "format": "uuid"
            }
          }
        ],
        "responses": {
          "204": {
            "description": "No Content."
          },
          "412": {
            "description": "Pre condition validation.",
            "content": {
              "text/plain": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/Notification"
                  }
                }
              },
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/Notification"
                  }
                }
              },
              "text/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/Notification"
                  }
                }
              }
            }
          },
          "404": {
            "description": "Not Found.",
            "content": {
              "text/plain": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/Notification"
                  }
                }
              },
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/Notification"
                  }
                }
              },
              "text/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/Notification"
                  }
                }
              }
            }
          },
          "500": {
            "description": "Internal server error."
          }
        }
      }
    },
    "/api/v1/characters/houses/{id}": {
      "get": {
        "tags": [
          "Characters by house"
        ],
        "summary": "Get all Characters by house.",
        "description": "Sample request:\r\n\r\n curl -X GET \"api/v1/characters/houses/5a05dc58d45bd0a11bd5e070\" -H \"accept: text/plain\"",
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "description": "",
            "required": true,
            "schema": {
              "type": "string",
              "description": "",
              "format": "string"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "A list of Characters.",
            "content": {
              "text/plain": {
                "schema": {
                  "$ref": "#/components/schemas/CharacterQueryResponse"
                }
              },
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/CharacterQueryResponse"
                }
              },
              "text/json": {
                "schema": {
                  "$ref": "#/components/schemas/CharacterQueryResponse"
                }
              }
            }
          },
          "404": {
            "description": "House not found.",
            "content": {
              "text/plain": {
                "schema": {
                  "$ref": "#/components/schemas/DefaultResponse"
                }
              },
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/DefaultResponse"
                }
              },
              "text/json": {
                "schema": {
                  "$ref": "#/components/schemas/DefaultResponse"
                }
              }
            }
          },
          "500": {
            "description": "Internal server error."
          }
        }
      }
    },
    "/api/v1/houses": {
      "get": {
        "tags": [
          "Houses"
        ],
        "summary": "Get all Houses.",
        "description": "Sample request:\r\n\r\n curl -X GET \"api/v1/houses\" -H \"accept: text/plain\"",
        "parameters": [],
        "responses": {
          "200": {
            "description": "A list of Houses.",
            "content": {
              "text/plain": {
                "schema": {
                  "$ref": "#/components/schemas/HousesQueryResponse"
                }
              },
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/HousesQueryResponse"
                }
              },
              "text/json": {
                "schema": {
                  "$ref": "#/components/schemas/HousesQueryResponse"
                }
              }
            }
          },
          "404": {
            "description": "Houses not found.",
            "content": {
              "text/plain": {
                "schema": {
                  "$ref": "#/components/schemas/DefaultResponse"
                }
              },
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/DefaultResponse"
                }
              },
              "text/json": {
                "schema": {
                  "$ref": "#/components/schemas/DefaultResponse"
                }
              }
            }
          },
          "500": {
            "description": "Internal server error."
          }
        }
      }
    },
    "/api/v1/houses/{id}": {
      "get": {
        "tags": [
          "Houses"
        ],
        "summary": "Get a single House.",
        "description": "Sample request:\r\n\r\n curl -X GET \"api/v1/houses/5a05dc58d45bd0a11bd5e070\" -H \"accept: text/plain\"",
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "description": "",
            "required": true,
            "schema": {
              "type": "string",
              "description": "",
              "format": "string"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "A single House.",
            "content": {
              "text/plain": {
                "schema": {
                  "$ref": "#/components/schemas/HouseQueryResponse"
                }
              },
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/HouseQueryResponse"
                }
              },
              "text/json": {
                "schema": {
                  "$ref": "#/components/schemas/HouseQueryResponse"
                }
              }
            }
          },
          "404": {
            "description": "House not found.",
            "content": {
              "text/plain": {
                "schema": {
                  "$ref": "#/components/schemas/DefaultResponse"
                }
              },
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/DefaultResponse"
                }
              },
              "text/json": {
                "schema": {
                  "$ref": "#/components/schemas/DefaultResponse"
                }
              }
            }
          },
          "500": {
            "description": "Internal server error."
          }
        }
      }
    }
  },
  "components": {
    "schemas": {
      "SelectCharacters": {
        "type": "object",
        "properties": {
          "id": {
            "type": "string",
            "nullable": false
          },
          "name": {
            "type": "string",
            "nullable": false
          },
          "role": {
            "type": "string",
            "nullable": false
          },
          "school": {
            "type": "string",
            "nullable": false
          },
          "house": {
            "type": "string",
            "nullable": false
          },
          "patronus": {
            "type": "string",
            "nullable": false
          }
        },
        "additionalProperties": false
      },
      "SelectCharactersByHouse": {
        "type": "object",
        "properties": {
          "id": {
            "type": "string",
            "nullable": false
          },
          "name": {
            "type": "string",
            "nullable": false
          },
          "role": {
            "type": "string",
            "nullable": false
          },
          "school": {
            "type": "string",
            "nullable": false
          },
          "house": {
            "type": "string",
            "nullable": false
          },
          "patronus": {
            "type": "string",
            "nullable": false
          }
        },
        "additionalProperties": false
      },
      "SelectCharacterById": {
        "type": "object",
        "properties": {
          "id": {
            "type": "string",
            "nullable": false
          },
          "name": {
            "type": "string",
            "nullable": false
          },
          "role": {
            "type": "string",
            "nullable": false
          },
          "school": {
            "type": "string",
            "nullable": false
          },
          "house": {
            "type": "string",
            "nullable": false
          },
          "patronus": {
            "type": "string",
            "nullable": false
          }
        },
        "additionalProperties": false
      },
      "CreateCharactersCommand": {
        "type": "object",
        "properties": {
          "name": {
            "type": "string",
            "nullable": false
          },
          "role": {
            "type": "string",
            "nullable": false
          },
          "school": {
            "type": "string",
            "nullable": false
          },
          "house": {
            "type": "string",
            "nullable": false
          },
          "patronus": {
            "type": "string",
            "nullable": false
          }
        },
        "additionalProperties": false
      },
      "CreateCharacterCommandResponse": {
        "type": "object",
        "properties": {
          "id": {
            "type": "string",
            "nullable": false
          },
          "name": {
            "type": "string",
            "nullable": false
          },
          "role": {
            "type": "string",
            "nullable": false
          },
          "school": {
            "type": "string",
            "nullable": false
          },
          "house": {
            "type": "string",
            "nullable": false
          },
          "patronus": {
            "type": "string",
            "nullable": false
          }
        },
        "additionalProperties": false
      },
      "CharactersQueryResponse": {
        "type": "object",
        "properties": {
          "characters": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/SelectCharacters"
            },
            "nullable": false
          }
        },
        "additionalProperties": false
      },
      "CharactersByHouseQueryResponse": {
        "type": "object",
        "properties": {
          "characters": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/SelectCharactersByHouse"
            },
            "nullable": false
          }
        },
        "additionalProperties": false
      },
      "CharacterQueryResponse": {
        "type": "object",
        "properties": {
          "character": {
            "$ref": "#/components/schemas/SelectCharacterById"
          }
        },
        "additionalProperties": false
      },
      "UpdateCharacterCommand": {
        "type": "object",
        "properties": {
          "id": {
            "type": "string",
            "format": "uuid",
            "readOnly": true
          },
          "name": {
            "type": "string",
            "nullable": false
          },
          "role": {
            "type": "string",
            "nullable": false
          },
          "school": {
            "type": "string",
            "nullable": false
          },
          "house": {
            "type": "string",
            "nullable": false
          },
          "patronus": {
            "type": "string",
            "nullable": false
          },
          "notifications": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/Notification"
            },
            "nullable": true,
            "readOnly": true
          },
          "invalid": {
            "type": "boolean",
            "readOnly": true
          },
          "valid": {
            "type": "boolean",
            "readOnly": true
          }
        },
        "additionalProperties": false
      },
      "UpdateCharacterCommandResponse": {
        "type": "object",
        "properties": {
          "id": {
            "type": "string",
            "format": "uuid",
            "readOnly": true
          },
          "name": {
            "type": "string",
            "nullable": false
          },
          "role": {
            "type": "string",
            "nullable": false
          },
          "school": {
            "type": "string",
            "nullable": false
          },
          "house": {
            "type": "string",
            "nullable": false
          },
          "patronus": {
            "type": "string",
            "nullable": false
          }
        },
        "additionalProperties": false
      },
      "DeleteCharacterCommand": {
        "type": "object",
        "properties": {
          "id": {
            "type": "string",
            "format": "uuid",
            "readOnly": true
          },
          "notifications": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/Notification"
            },
            "nullable": true,
            "readOnly": true
          },
          "invalid": {
            "type": "boolean",
            "readOnly": true
          },
          "valid": {
            "type": "boolean",
            "readOnly": true
          }
        },
        "additionalProperties": false
      },
      "SelectHouses": {
        "type": "object",
        "properties": {
          "_id": {
            "type": "string",
            "nullable": true
          },
          "name": {
            "type": "string",
            "nullable": true
          },
          "mascot": {
            "type": "string",
            "nullable": true
          },
          "headOfHouse": {
            "type": "string",
            "nullable": true
          },
          "houseGhost": {
            "type": "string",
            "nullable": true
          },
          "founder": {
            "type": "string",
            "nullable": true
          },
          "__v": {
            "type": "integer",
            "format": "int32",
            "nullable": true
          },
          "school": {
            "type": "string",
            "nullable": true
          },
          "members": {
            "type": "array",
            "items": {
              "type": "string"
            },
            "nullable": true
          },
          "values": {
            "type": "array",
            "items": {
              "type": "string"
            },
            "nullable": true
          },
          "colors": {
            "type": "array",
            "items": {
              "type": "string"
            },
            "nullable": true
          }
        },
        "additionalProperties": false
      },
      "SelectHouseById": {
        "type": "object",
        "properties": {
          "_id": {
            "type": "string",
            "nullable": true
          },
          "name": {
            "type": "string",
            "nullable": true
          },
          "mascot": {
            "type": "string",
            "nullable": true
          },
          "headOfHouse": {
            "type": "string",
            "nullable": true
          },
          "houseGhost": {
            "type": "string",
            "nullable": true
          },
          "founder": {
            "type": "string",
            "nullable": true
          },
          "__v": {
            "type": "integer",
            "format": "int32",
            "nullable": true
          },
          "school": {
            "type": "string",
            "nullable": true
          },
          "members": {
            "type": "array",
            "items": {
              "type": "string"
            },
            "nullable": true
          },
          "values": {
            "type": "array",
            "items": {
              "type": "string"
            },
            "nullable": true
          },
          "colors": {
            "type": "array",
            "items": {
              "type": "string"
            },
            "nullable": true
          }
        },
        "additionalProperties": false
      },
      "HousesQueryResponse": {
        "type": "object",
        "properties": {
          "characters": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/SelectHouses"
            },
            "nullable": false
          }
        },
        "additionalProperties": false
      },
      "HouseQueryResponse": {
        "type": "object",
        "properties": {
          "character": {
            "$ref": "#/components/schemas/SelectHouseById"
          }
        },
        "additionalProperties": false
      },
      "DefaultResponse": {
        "type": "object",
        "properties": {
          "message": {
            "type": "string",
            "nullable": true,
            "readOnly": true
          }
        },
        "additionalProperties": false
      },
      "Notification": {
        "type": "object",
        "properties": {
          "property": {
            "type": "string",
            "nullable": true,
            "readOnly": true
          },
          "message": {
            "type": "string",
            "nullable": true,
            "readOnly": true
          }
        },
        "additionalProperties": false
      }
    }
  }
}
