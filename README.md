## Harry Potter API

### Versões e dependências
- Ruby 2.6.0
- Rails 6.0.3
- Redis

### Clone o repositório
- git clone git@gitlab.com:isabelademattos/harry-potter-api.git
- cd harry-potter-api
- rake db:create db:migrate db:seed
- rails s
- Rodar aplicação em http://localhost:3000/

### Iniciar redis
- Em um novo terminal rodar
- redis-server

### Check-list
- Rota para criar novo personagem
- Rota para editar personagem
- Rota para excluir personagem
- Rota para listar todos os personagens
- Rota para listar todos os personagens de uma casa
- Rota para listar todas as casas vindas da API https://potterapi.com
- Testes gerais >> rspec spec
- Documentação da API no swagger disponível em http://localhost:3000/api-docs/index.html
- Validações para preenchimento de todos os campos do personagem
- Validação para não permitir salvar um personagem em uma casa inexistente
- Utilização do redis para busca das casas da API https://potterapi.com

### To-do-list
- Implementar API key
- Implementar circuit breaker
- Implementar docker
